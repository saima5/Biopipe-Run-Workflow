# Biopipe
A lightweight system enabling comparison of bioinformatics tools and workflows.

# Installation
Biopipe requires JAVA (JDK) 1.8 or later and Docker version 17.03.1-ce or later installed in the user's machine.

## Download Biopipe
You can download Biopipe directly from github and extract it. You can also download it using the following command:
```bash
git clone git@code.vt.edu:saima5/Biopipe-Run-Workflow.git
```
From now on, we will refer the Biopipe directory in the user's local machine as project directory. 
## Install Java
If Java is not already installed, you need to install Java (JDK) 1.8 or later from the following link: http://www.oracle.com/technetwork/java/javase/downloads/index.html. From this link, download the appropriate jdk installation file (for linux or macOS), and then install Java by double-clicking the downloaded installation file.
## Install Docker
If Docker is not already installed, you can download and install the latest version of Docker from the following link: https://docs.docker.com/engine/installation/

## Install Biopipe
In terminal, go into the project directory, which should contain `src` and `bin` folders. From the project directory, run the following command:
```bash
javac -cp gson-2.8.1.jar -d bin src/*.java
```
# Run Biopipe using test data
From the project directory, run the following command:
```bash
java -cp gson-2.8.1.jar:bin MainFrame
```
Running the above command will generate the GUI to select the xml file describing the workflow. A test xml file for variant-calling is given in `test-variant-calling-workflow` folder along with other input files. Here, the input files are:
1. *variant-calling-workflow.xml* : xml file containing the workflow (a similar workflow file can be created and downloaded from Biopipe web application http://bench.cs.vt.edu:8282/Biopipe-Workflow-Editor-0.0.1/index.xhtml)
2. *Homo_sapiens.GRCh38.dna.chromosome.21.fa* : reference genome
3. *test-read_1.fq* and *test-read_2.fq* : paired-end reads in fastq format 

You can run a variant calling workflow using these test input sets. As the workflow file contains instructions for running two alignment tools (Bowtie2 and BWA-MEM) and four variant-calling tools (Platypus, Freebayes, GATK-HaplotypeCaller, GATK-UnifiedGenotyper), all of these tools will run for the given test input sets and results will be generated in a user-defined directory.

Similarly, you can run a RNA-Seq workflow containing two RNA-seq alignment tools (STAR and Tophat2) and three RNA-Seq assembly tools (Cufflinks, StringTie, and Trinity-GenomeGuided) using the xml file *rnaseq-workflow.xml* given in the folder `test-rnaseq-workflow`. We need to provide three input files in order to run this workflow which are also given in the same folder. The input files are:
1. *rnaseq-workflow.xml* : xml file containing the workflow (a similar workflow file can be created and downloaded from Biopipe web application http://bench.cs.vt.edu:8282/Biopipe-Workflow-Editor-0.0.1/index.xhtml)
2. *TAIR10_Chr2.fasta* : reference genome
3. *test-reads.fq* : single-end reads
4. *Araport11_GFF3_genes_transposons.201606.gtf* : gtf file

After running the RNA-Seq workflow, all the outputs will be stored in a user-defined directory.

# Support
If you are having issues, please contact us at saima5@vt.edu
# License
This project is licensed under the BSD 2-clause "Simplified" License.
