/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author saima
 */
public class VariantCallingTools {
    private List<String> inputBamFileNames;
    private Map<String, String> toolImageMap;
    
    public VariantCallingTools() {
        inputBamFileNames = new ArrayList<String>();
        toolImageMap = new HashMap<String, String>();
    }
    
    public void addInputBamFile(String samFile) {
        inputBamFileNames.add(samFile);
    }

    public List<String> getInputBamFileNames() {
        return inputBamFileNames;
    }
    
    public void addToolImage(String toolName, String dockerImageName) {
        toolImageMap.put(toolName, dockerImageName);
    }

    public Map<String, String> getToolImageMap() {
        return toolImageMap;
    }
}
