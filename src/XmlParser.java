/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author saima
 */
public class XmlParser {
    public void parseXml(String xmlFilePath) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Workflow.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Workflow workflow = (Workflow) jaxbUnmarshaller.unmarshal(new File(xmlFilePath));
            if (workflow.getType().equals("variant-calling")) {
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new VariantCallingInputFrame(workflow).setVisible(true);
                    }
                });
            }
            else if (workflow.getType().equals("rna-seq")) {
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new RnaSeqInputFrame(workflow).setVisible(true);
                    }
                });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
