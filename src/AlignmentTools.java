/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saima
 */
public class AlignmentTools {
    private String read1Dir;
    private String read1FileName;
    private String read2Dir;
    private String read2FileName;
    private String referenceDir;
    private String referenceFileName;
    private String gtfAnnotationDir;
    private String gtfAnnotationFileName;
    private boolean isPaired;
    private String read1DockerDir;
    private String read2DockerDir;
    private String referenceDockerDir;
    private String gtfAnnotationDockerDir;
    private Map<String, String> toolImageMap;

    public AlignmentTools(String read1DockerDir, String read2DockerDir, String referenceDockerDir) {
        this.read1DockerDir = read1DockerDir;
        this.read2DockerDir = read2DockerDir;
        this.referenceDockerDir = referenceDockerDir;
        toolImageMap = new HashMap<String, String>();
    }
    
    public AlignmentTools(String read1DockerDir, String read2DockerDir, String referenceDockerDir, 
            String gtfAnnotationDockerDir) {
        this.read1DockerDir = read1DockerDir;
        this.read2DockerDir = read2DockerDir;
        this.referenceDockerDir = referenceDockerDir;
        this.gtfAnnotationDockerDir = gtfAnnotationDockerDir;
        toolImageMap = new HashMap<String, String>();
    }
    
    public String getRead1Dir() {
        return read1Dir;
    }

    public void setRead1Dir(String read1Dir) {
        this.read1Dir = read1Dir;
    }

    public String getRead1FileName() {
        return read1FileName;
    }

    public void setRead1FileName(String read1FileName) {
        this.read1FileName = read1FileName;
    }

    public String getRead2Dir() {
        return read2Dir;
    }

    public void setRead2Dir(String read2Dir) {
        this.read2Dir = read2Dir;
    }

    public String getRead2FileName() {
        return read2FileName;
    }

    public void setRead2FileName(String read2FileName) {
        this.read2FileName = read2FileName;
    }

    public String getReferenceDir() {
        return referenceDir;
    }

    public void setReferenceDir(String referenceDir) {
        this.referenceDir = referenceDir;
    }

    public String getReferenceFileName() {
        return referenceFileName;
    }

    public void setReferenceFileName(String referenceFileName) {
        this.referenceFileName = referenceFileName;
    }

    public boolean isPaired() {
        return isPaired;
    }

    public void setIsPaired(boolean isPaired) {
        this.isPaired = isPaired;
    }
    
    public void addToolImage(String toolName, String dockerImageName) {
        toolImageMap.put(toolName, dockerImageName);
    }

    public Map<String, String> getToolImageMap() {
        return toolImageMap;
    }

    public String getRead1DockerDir() {
        return read1DockerDir;
    }

    public void setRead1DockerDir(String read1DockerDir) {
        this.read1DockerDir = read1DockerDir;
    }

    public String getRead2DockerDir() {
        return read2DockerDir;
    }

    public void setRead2DockerDir(String read2DockerDir) {
        this.read2DockerDir = read2DockerDir;
    }

    public String getReferenceDockerDir() {
        return referenceDockerDir;
    }

    public void setReferenceDockerDir(String referenceDockerDir) {
        this.referenceDockerDir = referenceDockerDir;
    } 

    public String getGtfAnnotationDir() {
        return gtfAnnotationDir;
    }

    public void setGtfAnnotationDir(String gtfAnnotationDir) {
        this.gtfAnnotationDir = gtfAnnotationDir;
    }

    public String getGtfAnnotationFileName() {
        return gtfAnnotationFileName;
    }

    public void setGtfAnnotationFileName(String gtfAnnotationFileName) {
        this.gtfAnnotationFileName = gtfAnnotationFileName;
    }

    public String getGtfAnnotationDockerDir() {
        return gtfAnnotationDockerDir;
    }

    public void setGtfAnnotationDockerDir(String gtfAnnotationDockerDir) {
        this.gtfAnnotationDockerDir = gtfAnnotationDockerDir;
    }
}
